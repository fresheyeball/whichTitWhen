module Types exposing (..)

import Time


type Action
    = LeftBreast
    | RightBreast
    | Bottle
    | Pump
    | Poo
    | Pee
    | PooAndPee
    | DoneFeeding


type alias Feeding =
  { time : Time.Posix
  , action : Action
  }


type Msg
    = Add Action
    | Tick Time.Posix
    | Restore (List Feeding)
    | Delete Feeding
    | GotZone Time.Zone
    | Noop


type alias Model =
    { feedings : List Feeding
    , time : Time.Posix
    , zone : Time.Zone
    }
