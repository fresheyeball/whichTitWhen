module Main exposing (..)

import Browser
import Time exposing (utc)
import Task

import View exposing (view)
import Update exposing (update)
import Types exposing (Msg(..), Model)
import Persist


init : a -> b -> c -> ( Model, Cmd Msg )
init _ _ _ =
    ( { feedings = [], time = Time.millisToPosix 0, zone = utc }
    , Cmd.batch [Persist.restore, Task.perform GotZone Time.here]
    )


everySecond : Sub Msg
everySecond =
    Time.every 1000 Tick


main : Program () Model Msg
main = Browser.application
  { init = init
  , view = \m -> { title = "Which Tit When", body = [ view m ] }
  , update = update
  , subscriptions = always everySecond
  , onUrlRequest = always Noop
  , onUrlChange = always Noop
  }
