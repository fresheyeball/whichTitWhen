module View exposing (view)

import Html exposing (..)
import Html.Attributes as Attr
import Html.Events as Evt
import DateFormat exposing (format)
import Types exposing (..)
import String
import Time exposing (Posix, Zone)


css : Html Msg
css =
    let
        link_ package =
            let
                url =
                    "https://cdnjs.cloudflare.com/ajax/libs/"
                        ++ "semantic-ui/2.1.7/"
                        ++ package
                        ++ ".min.css"
            in
                node
                    "link"
                    [ Attr.rel "stylesheet"
                    , Attr.type_ "text/css"
                    , Attr.href url
                    ]
                    []
    in
        div
            []
            [ link_ "semantic"
            , link_ "components/table"
            , link_ "components/icon"
            , link_ "components/menu"
            ]


meta : String -> String -> Html Msg
meta name content =
    node
        "meta"
        [ Attr.attribute "name" name
        , Attr.attribute "content" content
        ]
        []


mobile : Html Msg
mobile =
    div
        []
        [ meta "viewport" "width=device-width, initial-scale=1"
        , meta "mobile-web-app-capable" "yes"
        , meta "apple-mobile-web-app-capable" "yes"
        , node "link" [ Attr.rel "apple-touch-icon"
                      , Attr.href "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Breastfeeding-icon-med.svg/2000px-Breastfeeding-icon-med.svg.png" ] []
        , node "link" [ Attr.rel "shortcut icon"
                      , Attr.href "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Breastfeeding-icon-med.svg/2000px-Breastfeeding-icon-med.svg.png" ] []
        ]


renderFeeding : Zone -> Feeding -> Html Msg
renderFeeding zone { time, action } =
    let
        ( message, icon ) =
            case action of
                LeftBreast ->
                    ( "Left", "caret left" )

                RightBreast ->
                    ( "Right", "caret right" )

                Bottle ->
                    ( "Bottle", "circle thin" )

                DoneFeeding ->
                    ( "Feed", "star" )

                Pump ->
                    ( "Pump", "filter" )

                Poo ->
                    ( "Poo", "sort descending" )

                Pee ->
                    ( "Pee", "sort ascending" )

                PooAndPee ->
                    ( "Poo & Pee", "sort" )
    in
        tr
            [ Attr.style "background" ( eventColor action )
            , Attr.style "color" "white" ]
            [ td
                [ Attr.class "collapsing" ]
                [ i
                    [ Attr.class <| "icon " ++ icon ]
                    []
                , text message
                ]
            , td
                []
                [ text <| format zone time ]
            , td
                [ Attr.class "collapsing" ]
                [ i
                    [ Attr.class <| "icon x"
                    , Attr.style "cursor" "pointer"
                    , Evt.onClick (Delete (Feeding time action ))
                    ]
                    []
                ]
            ]


format : Zone -> Posix -> String
format = DateFormat.format
  [ DateFormat.monthNameFull
  , DateFormat.text " "
  , DateFormat.dayOfMonthSuffix
  , DateFormat.text ", "
  , DateFormat.yearNumber
  , DateFormat.text " "
  , DateFormat.hourNumber
  , DateFormat.text ":"
  , DateFormat.minuteFixed
  , DateFormat.amPmLowercase
  ]


renderFeedings : Zone -> List Feeding -> Html Msg
renderFeedings zone feedings =
    div
        [ Attr.class "ui main text container"
        , Attr.style "margin-top" "65px"
        ]
        [ table
            [ Attr.class "ui celled striped table unstackable" ]
            [ tbody
                []
                (List.map (renderFeeding zone) feedings)
            ]
        ]


eventColor : Action -> String
eventColor lac =
    case lac of
        LeftBreast ->
            "#2185d0"

        RightBreast ->
            "#a333c8"

        Bottle ->
            "#f2711c"

        DoneFeeding ->
            "#767676"

        Pump ->
            "#C5769D"

        Pee ->
            "#DEA90B"

        Poo ->
            "#653800"

        PooAndPee ->
            "#7B7825"


toolBar : Html Msg
toolBar =
    let
        tool text_ lactation =
            div
                [ Attr.class ("item inverted")
                , Attr.style "color" "white"
                , Attr.style "background" (eventColor lactation)
                , Evt.onClick (Add lactation)
                ]
                [ text text_ ]
    in
        div
            [ Attr.class "ui eight item fixed menu"
            , Attr.style "cursor" "pointer"
            ]
            [ tool "Left Breast" LeftBreast
            , tool "Right Breast" RightBreast
            , tool "Bottle" Bottle
            , tool "Feed" DoneFeeding
            , tool "Poo" Poo
            , tool "Pee" Pee
            , tool "Poo & Pee" PooAndPee
            , tool "Pump" Pump
            ]


timer : Int -> Html Msg
timer mills =
    let
        seconds = remainderBy 60 (mills // 1000)
        minutes = remainderBy 60 (mills // (1000 * 60))
        hours   = remainderBy 24 (mills // (1000 * 60 * 60))

        fm = String.fromInt >> String.padLeft 2 '0'

        formated =
            String.join
                ":"
                [ fm hours
                , fm minutes
                , fm seconds
                ]

        style =
            [ Attr.style "bottom" "0"
            , Attr.style "left" "0"
            , Attr.style "position" "fixed"
            , Attr.style "padding" "15px"
            , Attr.style "background" "white"
            ]
    in if hours < 0 || minutes < 0 || hours < 0 then text "" else
        div
            ( Attr.class "ui item header" :: style)
            [ text <| "Since last feed: " ++ formated ]


since : List Feeding -> Posix -> Int
since feedings now =
    let
        lastDoneFeeding =
            let
                f { time, action } mdone =
                    case mdone of
                        Nothing ->
                            if action == DoneFeeding then
                                Just time
                            else
                                Nothing

                        _ ->
                            mdone
            in
                List.foldl f Nothing
    in
        Time.posixToMillis now - Time.posixToMillis (Maybe.withDefault now (lastDoneFeeding feedings))


view : Model -> Html Msg
view { feedings, time, zone } =
    div
        []
        [ mobile
        , css
        , toolBar
        , renderFeedings zone feedings
        , timer (since feedings time)
        ]
