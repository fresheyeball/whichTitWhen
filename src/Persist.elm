module Persist exposing (save, delete, restore)


import Http
import Time
import Json.Encode as E
import Json.Decode as D

import Types exposing (..)


decodeAction : D.Decoder Action
decodeAction = D.andThen (\x -> case x of
  "LeftBreast"  -> D.succeed LeftBreast
  "RightBreast" -> D.succeed RightBreast
  "Bottle"      -> D.succeed Bottle
  "Pump"        -> D.succeed Pump
  "Poo"         -> D.succeed Poo
  "Pee"         -> D.succeed Pee
  "PooAndPee"   -> D.succeed PooAndPee
  "DoneFeeding" -> D.succeed DoneFeeding
  _ -> D.fail "Not a valid action"
  ) D.string


encodeAction : Action -> E.Value
encodeAction a = E.string <| case a of
  LeftBreast -> "LeftBreast"
  RightBreast -> "RightBreast"
  Bottle -> "Bottle"
  Pump -> "Pump"
  Poo -> "Poo"
  Pee -> "Pee"
  PooAndPee -> "PooAndPee"
  DoneFeeding -> "DoneFeeding"


decodeTime : D.Decoder Time.Posix
decodeTime = D.map Time.millisToPosix D.int


encodeTime : Time.Posix -> E.Value
encodeTime = Time.posixToMillis >> E.int


decodeFeeding : D.Decoder Feeding
decodeFeeding = D.map2 Feeding
  (D.field "time" decodeTime)
  (D.field "action" decodeAction)


encodeFeeding : Feeding -> E.Value
encodeFeeding f = E.object
  [ ("time", encodeTime f.time)
  , ("action", encodeAction f.action)
  ]


save : Feeding -> Cmd Msg
save x = Http.post
  { url = "/actions"
  , body = Http.jsonBody <| encodeFeeding x
  , expect = Http.expectString (always Noop)
  }

delete : Time.Posix -> Cmd Msg
delete x = Http.request
  { method = "DELETE"
  , headers = []
  , url = "/actions?time=eq." ++ String.fromInt (Time.posixToMillis x)
  , body = Http.emptyBody
  , expect = Http.expectString (always Noop)
  , timeout = Nothing
  , tracker = Nothing
  }

restore : Cmd Msg
restore = Http.get
  { url = "/actions"
  , expect = Http.expectJson (\e -> case e of
    Err err -> Noop
    Ok x -> Restore x) (D.map (List.reverse << List.sortBy (Time.posixToMillis << .time)) <| D.list decodeFeeding)
  }
