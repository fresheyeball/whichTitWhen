module Update exposing (update)


import Persist
import Types exposing (..)


update : Msg -> Model -> ( Model, Cmd Msg )
update action { feedings, time, zone } =
    let
        feedings_ =
            case action of
                Add lactation ->
                    Feeding time_ lactation :: feedings

                Delete feeding ->
                    List.filter ((/=) feeding) feedings

                Restore feedingsFromStorage ->
                    feedingsFromStorage

                _ ->
                    feedings

        time_ =
            case action of
                Tick now ->
                    now

                _ ->
                    time

        zone_ =
            case action of
                GotZone z -> z
                _ -> zone

        model_ =
            { feedings = feedings_
            , time = time_
            , zone = zone_
            }

        effects_ =
            case action of
                Add lactation ->
                    Persist.save <| Feeding time_ lactation

                Delete feeding ->
                    Persist.delete <| feeding.time

                _ ->
                    Cmd.none
    in
        ( model_, effects_ )
