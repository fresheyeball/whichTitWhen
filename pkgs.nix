let

  rev-postgrest = "23ca27d27e6136e272bd15219f4c5422bf8a0541";
  postgrest-src = builtins.fetchTarball {
    url = "https://github.com/PostgREST/postgrest/archive/${rev-postgrest}.tar.gz";
  };

  rev-hasql-pool = "47d8bf48e00f9c5e9119781f80600a516f4709c5";
  hasql-pool-src = builtins.fetchTarball {
    url = "https://github.com/nikita-volkov/hasql-pool/archive/${rev-hasql-pool}.tar.gz";
  };

  rev = "401360e15b6b48f8e144a0062880b137ef7c532c";

  gitignore = pkgs.callPackage (pkgs.fetchFromGitHub
    { owner = "siers";
      repo = "nix-gitignore";
      rev = "4f2d85f2f1aa4c6bff2d9fcfd3caad443f35476e";
      sha256 = "1vzfi3i3fpl8wqs1yq95jzdi6cpaby80n8xwnwa8h2jvcw3j7kdz";
    }) {};

  pkgs = import (builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
  }) { overlays = [ (self: super:{
    gitignore = gitignore.gitignoreSource;
    haskellPackages = super.haskellPackages.override {
      overrides = hself: hsuper: with pkgs.haskell.lib; {
        hasql-pool = dontCheck (hself.callCabal2nix "hasql-pool" hasql-pool-src {});
        postgrest  = dontCheck (hself.callCabal2nix "postgrest" postgrest-src {});
          # .overrideAttrs (x: { buildInputs = x.buildInputs ++ [ pkgs.postgresql ]; });
        jose       = hself.callHackage "jose" "0.7.0.0" {};
      };
    };
  }) ]; };

in pkgs
