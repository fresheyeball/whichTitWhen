with (import ./pkgs.nix);

let
  mkDerivation =
    { srcs ? ./elm-srcs.nix
    , src
    , name
    , srcdir ? "./src"
    , targets ? []
    , versionsDat ? ./versions.dat
    }:
    stdenv.mkDerivation {
      inherit name src;

      buildInputs = [ elmPackages.elm elm2nix haskellPackages.postgrest ];

      buildPhase = pkgs.elmPackages.fetchElmDeps {
        elmPackages = import srcs;
        inherit versionsDat;
      };

      installPhase = let
        elmfile = module: "${srcdir}/${builtins.replaceStrings ["."] ["/"] module}.elm";
      in ''
        mkdir -p $out/share/doc
        ${lib.concatStrings (map (module: ''
          echo "compiling ${elmfile module}"
          elm make ${elmfile module} --output $out/index.html --docs $out/share/doc/${module}.json
        '') targets)}
      '';
    };
in mkDerivation {
  name = "which-tit-when";
  srcs = ./elm-srcs.nix;
  src = gitignore [] ./.;
  targets = ["Main"];
  srcdir = "./src";
}

