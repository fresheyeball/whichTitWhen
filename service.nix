{ config, lib, ...}: with lib;

let
  pkgs = import ./pkgs.nix;

  frontend = import ./default.nix;
  cfg = config.services.whichTitWhen;

  conf = pkgs.writeTextFile {
    name = "postgrest.conf";
    text = ''
      db-uri = "postgres://whichtitwhen:${cfg.dbPassword}@localhost:5432/whichtitwhen"
      db-schema = "api"
      db-anon-role = "whichtitwhen"
      db-pool = 3
      db-poot-timeout = 10

      server-host = "!4"
      server-port = ${toString cfg.port}
    '';
  };

  initSql = pkgs.writeTextFile {
    name = "init.sql";
    text = ''
      BEGIN;

      CREATE SCHEMA api;
      CREATE TYPE ACTION AS ENUM ('LeftBreast','RightBreast','Bottle','Pump','Poo','Pee','PooAndPee','DoneFeeding');
      CREATE TABLE api.actions (
        time BIGINT PRIMARY KEY,
        action ACTION NOT NULL
      );

      COMMIT;
    '';
  };

in
{
  options = {
    services.whichTitWhen = {

      enable = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Know which tit, and when
        '';
      };

      port = mkOption {
        type = types.int;
        default = 8080;
        description = ''
          Port to serve the stuff
        '';
      };

      statePath = mkOption {
        type = types.string;
        default = "/var/lib/whichTitWhen";
        description = ''
          Path to a folder where we can be stateful
        '';
      };

      dbPassword = mkOption {
        type = types.string;
        default = "liberta";
        description = ''
          Password to database
        '';
      };

      fqdn = mkOption {
        type = types.string;
        description = ''
          What is the fully qualified domain name nginx should listen for?
        '';
      };

    };

  };

  config = {

    services.postgresql.enable = true;
    services.nginx.virtualHosts = {
      "${cfg.fqdn}" = {
        forceSSL = true;
        enableACME = true;
        locations = {
          "/" = {
            root = "${frontend}";
          };
          "/actions" = {
            proxyPass = "http://localhost:${toString cfg.port}";
          };
        };

      };
    };

    users.users.whichTitWhen = {};

    systemd.services.whichTitWhen = with pkgs; let

      pg = config.services.postgresql;

    in mkIf cfg.enable {
      preStart = ''
        set -x
        PATH=$PATH:${makeBinPath [ sudo pg.package ]}
        mkdir -p ${cfg.statePath}
        psqld(){
          sudo psql -U ${pg.superUser} -d template1 -c "$1"
        }
        if ! test -e "${cfg.statePath}/.db-created"; then
          echo "creating le database"
          psqld "CREATE DATABASE whichtitwhen"
          echo "creating user whichtitwhen"
          psqld "CREATE USER whichtitwhen WITH PASSWORD '${cfg.dbPassword}'"
          echo "granting le privilege"
          psqld "GRANT ALL PRIVILEGES ON DATABASE whichtitwhen TO whichtitwhen"
          psql -U whichtitwhen -d whichtitwhen -f ${initSql}
          touch ${cfg.statePath}/.db-created
        fi
      '';

      serviceConfig = {
        ExecStart = pkgs.writeShellScriptBin "whichTitWhen.sh" ''
          PATH=$PATH:${makeBinPath [ sudo pg.package pkgs.haskellPackages.postgrest ]}
          postgrest ${conf}
        '' + "/bin/whichTitWhen.sh";
      };

      wantedBy = [ "multi-user.target" ];

      after = [ "network.target" "postgresql.service" ];
    };

  };
}
